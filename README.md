# CHEEKERS #

### What is this repository for? ###

* This was made sophomore year in a team including Caroline Crossland, Sarah Fordin, Andrew Loder, and Drew Sposeep
* A checkers game with no double jumping and a Min-Max AI

### How do I get set up? ###

* Pull
* Install Haskell: https://www.haskell.org/
* Navigate to source directory 
* compile cheekers.hs

### How to Play? ###

* -h or help in console will put you in the right direction