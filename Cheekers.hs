import Text.Read
import Data.Char
import Data.Maybe
import System.IO
import System.Directory
import System.Environment
import System.Console.GetOpt
data Player = PlayerWhite | PlayerBlack deriving (Show, Eq, Read)
data Piece = Pawn Player | King Player deriving (Show, Eq, Read)
data BlackTile = BlackTile (Maybe Piece) deriving (Show, Eq, Read)
data Result = Player Player | Tie deriving (Show, Eq)
--TODO Implement these
data Flag = Help | Winner | Depth String | MoveFlag String | Verbose | Interactive | PlayerFlag deriving (Eq, Show)


type Move = (Position, Position)
type Board = [[Maybe BlackTile]]
type Position = (Integer, Integer)
type TurnNumber = Integer
type Game = (Board, Player,TurnNumber)



options :: [OptDescr Flag]
options = [
      Option ['h'] ["help"] (NoArg Help) "Print a help message and quit the program.",
      Option ['w'] ["winner"] (NoArg Winner) "Print who will win using an exhaustive search.",
      Option ['d'] ["depth"] (ReqArg (\s -> Depth s) "num") "Use <num> as a cutoff depth.",
      Option ['m'] ["move"] (ReqArg (\s -> MoveFlag s) "Move") "Make a move and print resulting board.",
      Option ['v'] ["verbose"] (NoArg Verbose) "Output move and a description of the moves quality.",
      Option ['i'] ["interactive"] (NoArg Interactive) "Start a new game and play against computer.",
      Option ['p'] ["player"] (NoArg PlayerFlag) "Start a new game and play against another player."
      ]

startBoard::Board
startBoard = let blankBoardInf = (cycle [Nothing,Just (BlackTile Nothing)])
                 whitePlayerInf = playerBoard PlayerWhite
                 blackPlayerInf= playerBoard PlayerBlack
                in (group 8 ((take 8 blackPlayerInf)++(tail (take 9 blackPlayerInf)++(take 8 blackPlayerInf))
                                 ++(tail (take 9 blankBoardInf))++(take 8 blankBoardInf)
                                 ++(tail (take 9 whitePlayerInf))++(take 8 whitePlayerInf)++(tail (take 9 whitePlayerInf))))
                where
                    playerBoard::Player->[Maybe BlackTile]
                    playerBoard player =
                      (cycle [Nothing,Just (BlackTile (Just (Pawn player)))])
                    group :: Int -> [a] -> [[a]]
                    group _ [] = []
                    group n l
                      | n > 0 = (take n l) : (group n (drop n l))
                      | otherwise = error "Negative n"


findDepth::[Flag]->Integer
findDepth [] = 4
findDepth ((Depth x):xs) = read x
findDepth (x:xs) = findDepth xs



startGame = (startBoard, PlayerWhite, 0)

main:: IO ()
main = do
    args <- getArgs
    let (flags, others, errors) = getOpt Permute options args
    if Help `elem` flags
    then
        putStrLn $ usageInfo "Cheekers" options
    else
      if Interactive `elem` flags
      then
        if (length others) == 1
          then do
            putStrLn (head others)
            ioGame <- (inputGame (head others))
            case ioGame of
              Just game -> computerGameLoop game (findDepth flags)
              Nothing -> putStrLn "Input a valid file."
        else computerGameLoop startGame (findDepth flags)
      else
        if PlayerFlag `elem` flags
        then
          if (length others) == 1
          then do
            ioGame <- (inputGame (head others))
            case ioGame of
              Just game -> playerGameLoop game
              Nothing -> putStrLn "Input a valid file."
          else playerGameLoop startGame
        else
          if (length others) == 1 then do
            ioGame <- (inputGame (head others))
            case ioGame of
              Just game -> mainLoop flags game
              Nothing -> putStrLn "Input a valid file."
          else putStrLn $ "You need to pass in a text file"




mainLoop::[Flag]->Game->IO ()
mainLoop flags game =
  if Winner `elem` flags
    then handleGameFlag Winner game
    else
      if Verbose `elem` flags
      then
        case (move game (optimalMove game (findDepth flags))) of
            Just g -> putStrLn $ getGameState g
            Nothing -> putStrLn "Invalid Optimal Move"
      else
        case getMove flags of
            Just (MoveFlag m) -> case (move game (read m)) of
                        Just e-> putStrLn $ getGameState e
                        Nothing -> putStrLn "Invalid Move"
            Nothing -> case (move game (optimalMove game (findDepth flags))) of
                        Just e-> putStrLn $ getGameState e
                        Nothing -> putStrLn "Invalid Move"


getMove::[Flag]->(Maybe Flag)
getMove [] = Nothing
getMove (move@(MoveFlag _):xs) = Just move
getMove (x:xs) = getMove xs

handleGameFlag::Flag->Game->IO ()
handleGameFlag Winner game =
        case (whoWillWin game) of
            Tie -> putStrLn "Tie"
            Player p -> putStrLn ((show p) ++ " will win")
handleGameFlag (MoveFlag realMove) game =
          case move game (read realMove) of
              Just g -> putStrLn $ getGameState g
              _ -> putStrLn "Invalid Move."
handleGameFlag _ game = putStrLn "invalid flag"

computerGameLoop:: Game ->Integer-> IO ()
computerGameLoop curGame@(board, player, turn) depth = do
  if player == PlayerBlack then
    case (move curGame (optimalMove curGame depth)) of
      Just g -> computerGameLoop g depth
      Nothing -> putStrLn "Computer Glitch terminating loop"
  else
    case (winner curGame) of
      Just Tie -> putStrLn "Its a tie"
      Just p -> case p of
                (Player PlayerWhite) -> putStrLn "White player wins"
                (Player PlayerBlack) -> putStrLn "Red player wins"
      _ -> do
            putStrLn "You are up!"
            putStr (getGameState curGame)
            putStrLn "Select Piece X Y"
            s1 <- getLine
            if ((all isDigit (map head (words s1))) /= True)&&length (words s1)== 2 then do
                putStrLn "invalid digits"
                computerGameLoop curGame depth
                else return ()
            putStrLn "Move Piece X Y"
            s2 <- getLine
            if ((all isDigit (map head (words s2))) /= True)&&length (words s2) == 2 then
              do
                putStrLn "Invalid digits"
                computerGameLoop curGame depth
                else return ()
            let p1 = (read (head (words s1)),read (head(tail (words s1))))
            let p2 = (read (head (words s2)),read (head(tail (words s2))))
            let game2 = move curGame (p1, p2)
            case game2 of
              Just g ->
                do
                  putStrLn "You moved"
                  computerGameLoop g depth
              Nothing ->
                do
                  putStrLn "Invalid Move"
                  computerGameLoop curGame depth

playerGameLoop:: Game -> IO ()
playerGameLoop curGame@(board, player, turn) =
  do
    putStr (getGameState curGame)
    case winner curGame of
      Just Tie -> putStrLn "Its a tie"
      Just p -> case p of
                (Player PlayerWhite) -> putStrLn "White player wins"
                (Player PlayerBlack) -> putStrLn "Red player wins"
      _ -> do
            if player == PlayerWhite then putStrLn "White is up"
               else putStrLn "Red is up"
            putStrLn "Select Piece X Y"
            s1 <- getLine
            if ((all isDigit (map head (words s1))) /= True)&&length (words s1)== 2 then do
                putStrLn "invalid digits"
                playerGameLoop curGame
                else return ()
            putStrLn "Move Piece X Y"
            s2 <- getLine
            if ((all isDigit (map head (words s2))) /= True)&&length (words s2) == 2 then
              do
                putStrLn "Invalid digits"
                playerGameLoop curGame
                else return ()
            let p1 = (read (head (words s1)),read (head(tail (words s1))))
            let p2 = (read (head (words s2)),read (head(tail (words s2))))
            let game2 = move curGame (p1, p2)
            case game2 of
              Just g ->
                do
                  putStrLn "You moved"
                  playerGameLoop g
              Nothing ->
                do
                  putStrLn "Invalid Move"
                  playerGameLoop curGame

--given a game state and two positions: the position of the current piece and the position of the requested move, it will return whether or not
validMove::Game -> Position -> Position -> Bool
validMove game@(board, player, _) p1@(x1,y1) p2@(x2,y2)  =
  case (getPiece game p1) of
      Just (Pawn p) ->           ((player == p)&& --Checks to see if that is the players piece
                                 (if p == PlayerWhite then (y1>y2) else (y1<y2))&& --Checks if it is a valid move for a regular piece or if it is a king
                                 (((isDiagonal p1 p2)&&(not (isPieceThere game p2)))|| -- if it is diagnal and there is not a piece at that position or...
                                 (validJump game (Pawn p) p1 p2))) --if the jump is valid

      Just (King p) ->           ((player == p)&& --Checks to see if that is the players piece
                                 (((isDiagonal p1 p2)&&(not (isPieceThere game p2)))|| -- if it is diagnal and there is not a piece at that position or...
                                 (validJump game (King p) p1 p2))) --if the jump is valid
      Nothing -> False -- if there is no piece return false

--Checks if two positions are isDiagonal
isDiagonal::Position->Position->Bool
isDiagonal (x1, y1) (x2,y2) = ((((x1-1) == x2 || (x1+1) == x2))&&((y1-1) == y2 || (y1+1) == y2))

--Given a gamestate and a position it will find if there is a piece at that position
isPieceThere::Game->Position->Bool
isPieceThere game@(board,_,_) (x,y)= case (getPiece game (x,y)) of
                                        Nothing -> False
                                        _ -> True
--validJump takes in a game state the piece that is trying to jump its current position and the requested position
validJump::Game->Piece->Position->Position->Bool
validJump game@(board, player, _) piece p1@(x1,y1) p2@(x2,y2) =
  case piece of
    Pawn p ->           (if p == PlayerWhite then (y1>y2) else (y1<y2))&&
                        ((getPiece game (adjacentX,adjacentY) == Just (Pawn (changePlayer p)))||
                        (getPiece game (adjacentX,adjacentY) == Just (King (changePlayer p))))&&
                        (isDiagonal (adjacentX,adjacentY) p2)&&
                        (not (isPieceThere game p2))
    King p ->           ((getPiece game (adjacentX,adjacentY) == Just (Pawn (changePlayer p)))||
                        (getPiece game (adjacentX,adjacentY) == Just (King (changePlayer p))))&&
                        (isDiagonal (adjacentX,adjacentY) p2)&&
                        (not (isPieceThere game p2))
    where
      adjacentX = if x2> x1 then x1+1 else x1-1
      adjacentY = if y2>y1 then y1+1 else y1-1


--gets the piece at the Position
getPiece::Game->Position->Maybe Piece
getPiece (board,_,_) (x,y) = if (((length board) > (fromIntegral y)) && ((length (head board)) > (fromIntegral x))&&(x>=0)&&(y>=0)) then
                              case ((board !! (fromIntegral y)) !! (fromIntegral x)) of
                                Just (BlackTile piece) -> piece
                                _ -> Nothing
                              else
                                Nothing
move :: Game->Move -> Maybe Game
move game@(board, player, turn) (curr, dest) = if((validMove game curr dest)==False) then Nothing
                                      else (Just (movePiece board curr dest jump tile 0, changePlayer player, turn+1))
                                      where jump = checkJumps game curr dest
                                            tile = checkKing game (BlackTile (getPiece game curr)) dest
--moves a piece given a board, current piece Position, the destination, and
movePiece :: Board -> Position -> Position -> Position -> BlackTile -> Integer -> Board
movePiece [] curr dest jump tile index = []
movePiece (x:xs) curr dest jump tile index | snd curr == index = (removeTile x 0 curr):(movePiece xs curr dest jump tile (index+1))
                                           | snd dest == index = (addTile x tile dest 0):(movePiece xs curr dest jump tile (index+1))
                                           | snd jump == index = (removeTile x 0 jump):(movePiece xs curr dest jump tile (index+1))
                                           | otherwise = x:(movePiece xs curr dest jump tile (index+1))
--Helper function for move constructs a list of tiles and blanks out theJust (BlackTile (Just (Pawn PlayerBlack))) one at the given index
removeTile :: [Maybe BlackTile] -> Integer -> Position -> [Maybe BlackTile]
removeTile (x:xs) index remove | fst remove == index = (Just (BlackTile Nothing)):xs
                               | otherwise = x:(removeTile xs (index+1) remove)
--Helper fuction for move constructs the x list of tiles and replaces the destanation with the given tile
addTile :: [Maybe BlackTile] -> BlackTile -> Position -> Integer -> [Maybe BlackTile]
addTile (x:xs) tile dest index | fst dest == index = (Just tile):xs
                               | otherwise = x:(addTile xs tile dest (index+1))
--Checks if the piece jumps and will return a position to empty that tile if no jump is found it will return (-1,-1)
checkJumps :: Game -> Position -> Position -> Position
checkJumps game@(board,player,_) curr@(x1,y1) dest@(x2,y2) | getPiece game pos /= Nothing = pos
                                                         | otherwise = (-1, -1)
                                                        where pos = (if x2> x1 then x1+1 else x1-1, if y2>y1 then y1+1 else y1-1)
--Checks if the tile already has a king or if the move should king the piece
checkKing:: Game->BlackTile->Position->BlackTile
checkKing (board,_,_) pawn@(BlackTile (Just (Pawn p))) dest =
                                        let y = snd dest
                                            boardEnd = fromIntegral ((length board)-1)
                                            king = BlackTile (Just (King p))
                                        in
                                         if p == PlayerBlack
                                          then if y == boardEnd then king
                                               else pawn
                                          else if y == 0 then king
                                               else pawn
checkKing _ ret _ = ret
--Change Players
changePlayer :: Player -> Player
changePlayer player | player == PlayerWhite = PlayerBlack
                    | player == PlayerBlack = PlayerWhite

--takes a file name and reutnrs a Maybe Game
inputGame :: String -> IO (Maybe Game)
inputGame fName = do
    handle <- openFile fName ReadMode
    contents <- hGetContents handle
    let ret = readGame contents
    return ret

--takes a string and returns a Maybe Game
readGame :: String -> Maybe Game
readGame contents =
    case lines contents of
    (plLine:turnLine:boardLines) -> do
          player <- readPlayer plLine
          turn <- readTurn turnLine
          board <- readBoard boardLines
          Just (reverse board,player,turn)
    _ -> Nothing

--takes a string and returns a Maybe Integer
readTurn :: String -> Maybe Integer
readTurn num = case readMaybe num of
                Just num -> return num
                Nothing -> Nothing

--takes a list of strings and returns a Maybe Board
readBoard :: [String] -> Maybe Board
readBoard board = if(elem Nothing maybeBoard) then Nothing else Just (catMaybes maybeBoard)
                where maybeBoard = readMaybeBoard board

readMaybeBoard :: [String] -> [Maybe [Maybe BlackTile]]
readMaybeBoard [] = []
readMaybeBoard (x:xs) = (readRow x):(readMaybeBoard xs)

--takes a string and returns a Maybe list of tiles
readRow :: String -> Maybe [Maybe BlackTile]
readRow row = if(elem Nothing maybeRows) then Nothing else Just (catMaybes maybeRows)
            where maybeRows = readMaybeRow (words row)

readMaybeRow :: [String] -> [Maybe (Maybe BlackTile)]
readMaybeRow [] = []
readMaybeRow (x:xs) | x == "W" = (Just (Nothing)):(readMaybeRow xs)
               | x == "B" = (Just (Just (BlackTile Nothing))):(readMaybeRow xs)
               | x == "PW" = (Just (Just (BlackTile (Just (Pawn PlayerWhite))))):(readMaybeRow xs)
               | x == "PB" = (Just (Just (BlackTile (Just (Pawn PlayerBlack))))):(readMaybeRow xs)
               | x == "KW" = (Just (Just (BlackTile (Just (King PlayerWhite))))):(readMaybeRow xs)
               | x == "KB" = (Just (Just (BlackTile (Just (King PlayerBlack))))):(readMaybeRow xs)
               | otherwise = Nothing:(readMaybeRow xs)

--takes a string and returns a player
readPlayer :: String -> Maybe Player
readPlayer p | p == "PlayerBlack" = Just PlayerBlack
             | p == "PlayerWhite" = Just PlayerWhite
             | otherwise = Just PlayerBlack

--writes a given game to a given file
writeGame :: Game -> String -> IO()
writeGame game@(board, player, turn) fName = do
    let file = fName
    handle <- openFile file WriteMode
    (tempName, tempHandle) <- openTempFile "." "temp"
    hPutStr tempHandle (showGame game)
    hClose handle
    hClose tempHandle
    removeFile file
    renameFile tempName file

--takes a game and returns a string
showGame :: Game -> String
showGame game@(board, player, turn) =
        (showPlayer player) ++ "\n" ++
        (show turn) ++ "\n" ++
        (unlines (showBoard (reverse board)))

--takes a player and returns a string
showPlayer :: Player -> String
showPlayer player | player == PlayerBlack = "PlayerBlack"
                   | player == PlayerWhite = "PlayerWhite"

--takes a board and returns a list of rows
showBoard :: [[Maybe BlackTile]] -> [String]
showBoard [] = []
showBoard (x:xs) = (showRow x):(showBoard xs)

--takes a row and returns a string of letters
showRow :: [Maybe BlackTile] -> String
showRow [] = []
showRow (x:xs) | x == (Nothing) = "W "++(showRow xs)
               | x == (Just (BlackTile Nothing)) = "B "++(showRow xs)
               | x == (Just (BlackTile (Just (Pawn PlayerWhite)))) = "PW "++(showRow xs)
               | x == (Just (BlackTile (Just (Pawn PlayerBlack)))) = "PB "++(showRow xs)
               | x == (Just (BlackTile (Just (King PlayerWhite)))) = "KW "++(showRow xs)
               | x == (Just (BlackTile (Just (King PlayerBlack)))) = "KB "++(showRow xs)

winner:: Game -> (Maybe Result)
winner game@(board, player, turn) = if ( turn <= 70) then
                    if ((fst whitePieces+ snd whitePieces) == 0) then
                           Just (Player PlayerBlack)
                    else
                         if ((fst blackPieces + snd blackPieces) == 0) then
                           Just (Player PlayerWhite)
                         else
                           Nothing
                 else Just Tie
                      where
                        --Tuple of (regular pieces, Kings)::(Integer,Integer)
                        whitePieces = countWhitePieces game
                        blackPieces = countBlackPieces game


--Prints all of the game variables including the board
getGameState:: Game -> String
getGameState game@(board,player,turn) = (getGameBoard game)++(show player)++"\'s turn\n"
                                        ++"White has: " ++(show (fst whitePieces)) ++ " pieces and " ++ (show (snd whitePieces)) ++ " kings\n"++
                                        "Red has: " ++(show (fst blackPieces)) ++ " pieces and " ++ (show (snd blackPieces)) ++ " kings\n"++(show turn)++" Turns so far \n"
                                        ++"Rating of the board: " ++(show (evalBoard game))++"\n"
                                          where
                                              whitePieces = countWhitePieces game
                                              blackPieces = countBlackPieces game

--converts the game board to a printable string rotates the board so that the board is indexed 0,0 at the bottom left
getGameBoard::Game -> String
getGameBoard (board,_,_) = foldr (++) "\n" ([ (show n)++
                          ([foldl (\acc y -> (convertTileToString y)++acc ) "\n" r | r <- (map reverse (board))]!!n) |n<-[(length board)-1,(length board)-2..0]]
                          ++" ":(map (\y -> " "++(show y)++" ") [0..((length board)-1)]))

--converts the Tile to a string representation for printing
convertTileToString:: Maybe BlackTile -> String
convertTileToString (Just (BlackTile Nothing)) =  "\x1b[40m"++"\x1b[30m"++" ■ "++"\x1b[0m"
convertTileToString (Just (BlackTile (Just (Pawn PlayerBlack)))) = "\x1b[40m"++"\x1b[31m"++" ● "++"\x1b[0m"
convertTileToString (Just (BlackTile (Just (King PlayerBlack)))) = "\x1b[40m"++"\x1b[31m"++" ◈ "++"\x1b[0m"
convertTileToString (Just (BlackTile (Just (Pawn PlayerWhite)))) = "\x1b[40m"++"\x1b[97m"++" ● "++"\x1b[0m"
convertTileToString (Just (BlackTile (Just (King PlayerWhite)))) = "\x1b[40m"++"\x1b[97m"++" ◈ "++"\x1b[0m"
convertTileToString (Nothing) = "\x1b[101m"++"\x1b[91m"++" □ "++"\x1b[0m"

--Counts the total regular black pieces and the kings
countBlackPieces:: Game -> (Integer, Integer)
countBlackPieces (board, _,_) = foldr (\(pieces, kings) (pieces2,kings2) -> (pieces+pieces2,kings+kings2)) (0,0) ([aux x (0,0)| x <- board])
                              where aux::[Maybe BlackTile]-> (Integer,Integer) -> (Integer,Integer)
                                    aux [] i = i
                                    aux (x:xs) (pieces,kings) = case x of
                                                              Just (BlackTile p) -> case p of
                                                                                    (Just (Pawn PlayerBlack)) -> aux xs (pieces+1, kings)
                                                                                    (Just (King PlayerBlack)) -> aux xs (pieces,kings+1)
                                                                                    _ -> aux xs (pieces,kings)
                                                              _ -> aux xs (pieces,kings)

--Counts the total regular white pieces and the kings
countWhitePieces:: Game -> (Integer, Integer)
countWhitePieces (board, _,_) = foldr (\(pieces, kings) (pieces2,kings2) -> (pieces+pieces2,kings+kings2)) (0,0) ([aux x (0,0)| x <- board])
                              where aux::[Maybe BlackTile]-> (Integer,Integer) -> (Integer,Integer)
                                    aux [] i = i
                                    aux (x:xs) (pieces,kings) = case x of
                                                              Just (BlackTile p) -> case p of
                                                                              (Just (Pawn PlayerWhite)) -> aux xs (pieces+1, kings)
                                                                              (Just (King PlayerWhite)) -> aux xs(pieces,kings+1)
                                                                              _ -> aux xs (pieces,kings)
                                                              _ -> aux xs (pieces,kings)






allValidMoves::Game-> [Move]
allValidMoves game@(board, player, turn) = aux game (map fst (getAllPlayablePieces game))
                                        where
                                           aux::Game->[Position]->[Move]
                                           aux game [] = []
                                           aux game@(board,player,turn) (pos@(x,y):poss) =
                                                  let validDMove = filter (\pos2 -> (validMove game pos pos2)&&(isNotOutsideBounds game pos2)) (map (\(x2,y2) -> (x+x2,y+y2)) [(-1,-1),(1,-1),(-1,1),(1,1)])
                                                      validJumps = filter (\pos2 -> (validMove game pos pos2)&&(isNotOutsideBounds game pos2)) (map (\(x2,y2) -> (x+x2,y+y2)) [(-2,-2),(2,-2),(-2,2),(2,2)])
                                                  in
                                                      ((map (\pos2 -> (pos, pos2)) validDMove)++(map (\pos2 -> (pos, pos2)) validJumps))++(aux game poss)
getAllPlayablePieces::Game->[(Position,Piece)]
getAllPlayablePieces game@(board,player,turn) = [((x,y),fromJust piece) | x<-[0..(fromIntegral (length (head board)))],y<-[0..(fromIntegral (length board))],let piece = (getPiece game (x,y)), isPlayersPiece piece player]

getAllPieces::Game->[(Position,Piece)]
getAllPieces game@(board,player,turn) = [((x,y),fromJust piece) | x<-[0..(fromIntegral (length (head board)))],y<-[0..(fromIntegral (length board))],let piece = (getPiece game (x,y)), piece /= Nothing]


isPlayersPiece::Maybe Piece->Player->Bool
isPlayersPiece piece player | piece == Just(Pawn player) = True
                            | piece == Just(King player) = True
                            | otherwise = False

isNotOutsideBounds::Game->Position->Bool
isNotOutsideBounds game@(board,_,_) (x,y) = (x < (fromIntegral(length (head board))))&&(y < (fromIntegral(length board)))&&(y > (-1))&&(x > (-1))

evalBoard :: Game -> Integer
evalBoard game =
  case winner game of
    Just Tie -> 0
    Just (Player p) -> if PlayerWhite == p then 24 else -24
    Nothing -> evalPlayer(countWhitePieces game) - evalPlayer (countBlackPieces game)
      where
          evalPlayer :: (Integer, Integer) -> Integer
          evalPlayer (a, b) = a + 2*b

--evaluate all the board states and decide which move is the best move 'Winning the game' is the best board state
optimalMove::Game->Integer->Move
optimalMove game@(board,player,turn) depth =
    let moves = allValidMoves game
        gameMoves = mapMaybe unMaybeFst [(move game m, m) | m <- moves]
        resultMoves = [(rateMoves g player (depth-1), m) | (g,m) <- gameMoves]
    in snd $ optimalForTuple player resultMoves

rateMoves::Game->Player->Integer->Integer
rateMoves game p 0 = evalBoard game
rateMoves game p depth =
  case winner game of
       Just r -> evalBoard game
       Nothing -> let allMoves = allValidMoves game
                      allGames = mapMaybe (move game) allMoves
                      allResults = [rateMoves g p (depth-1) | g<-allGames]
                  in optimalFor p allResults

optimalFor::Player->[Integer]->Integer
optimalFor PlayerWhite results = foldr1 max results
optimalFor PlayerBlack results = foldr1 min results

optimalForTuple::Player->[(Integer,Move)]->(Integer,Move)
optimalForTuple (PlayerWhite) results = foldr1 (\one@(x1,_) two@(x2,_) -> if x1>x2 then one else two) results
optimalForTuple (PlayerBlack) results = foldr1 (\one@(x1,_) two@(x2,_) -> if x1<x2 then one else two) results

--Helper function
unMaybeFst::(Maybe a,b) -> Maybe (a,b)
unMaybeFst (Just a,b) = Just (a,b)
unMaybeFst (Nothing,b) = Nothing


--Given a game state, search for a move that can force a win for the current player.Failing that, return a move that can force a tie for the current player.
bestMove::Game->Move
bestMove game@(board,player,turn) =
    let moves = allValidMoves game
        gameMoves = mapMaybe unMaybeFst [(move game m, m) | m <- moves]
        resultMoves = [(whoWillWin g, m) | (g,m) <- gameMoves]
    in snd $ bestForTuple player resultMoves


--Decides who will win based on if there is a path that can force a win
whoWillWin ::Game-> Result
whoWillWin game@(_,player,_) =
    case winner game of
         Just r -> r
         Nothing -> let allMoves = allValidMoves game
                        allGames = mapMaybe (move game) allMoves
                        allResults = map whoWillWin allGames
                    in bestFor player allResults


--given a list of results it will return the result that is best for the given player
bestFor::Player->[Result]->Result
bestFor p results = foldr1 maxRes results
    where
    maxRes res1 res2 | res1 == (Player p) = res1
                     | res2 == (Player p) = res2
                     | res1 == (Tie) = res1
                     | res2 == (Tie) = res2
                     | otherwise = res1

--Helper function for bestFor
bestForTuple::Player->[(Result,Move)]->(Result,Move)
bestForTuple p moveResults = foldr1 maxPos moveResults
  where
    maxPos p1@(res1,_) p2@(res2,_) | res1 == (Player p) = p1
                                   | res2 == (Player p) = p2
                                   | res1 == (Tie) = p1
                                   | res2 == (Tie) = p2
                                   | otherwise = p1
